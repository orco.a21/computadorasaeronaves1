package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio2 {

	private JFrame frame;
	private JTextField txt;
	private JLabel lblResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio2 window = new Ejercicio2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.GRAY);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNumero = new JLabel("inserte un numero ->");
		lblNumero.setFont(new Font("Calibri", Font.PLAIN, 20));
		lblNumero.setForeground(new Color(0, 0, 0));
		lblNumero.setBounds(34, 33, 213, 25);
		frame.getContentPane().add(lblNumero);
		
		txt = new JTextField();
		txt.setBounds(236, 33, 123, 27);
		frame.getContentPane().add(txt);
		txt.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int numero = Integer.parseInt(txt.getText());
				
				if (numero%2==0)
					lblResultado.setText("Par");
				
				else 
					lblResultado.setText("Impar");
					
			}
		});
		btnCalcular.setFont(new Font("Calibri", Font.BOLD, 16));
		btnCalcular.setBounds(76, 121, 112, 31);
		frame.getContentPane().add(btnCalcular);
		
		lblResultado = new JLabel("       Resultado");
		lblResultado.setOpaque(true);
		lblResultado.setBackground(Color.WHITE);
		lblResultado.setFont(new Font("Calibri", Font.PLAIN, 16));
		lblResultado.setForeground(new Color(0, 0, 0));
		lblResultado.setBounds(211, 121, 123, 31);
		frame.getContentPane().add(lblResultado);
	}

}
