package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio3 {

	private JFrame frame;
	private JTextField txt;
	private JLabel lblResultado;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio3 window = new Ejercicio3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblIngresarNumeroDel = new JLabel("Ingresar numero del mes ->");
		lblIngresarNumeroDel.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 20));
		lblIngresarNumeroDel.setBounds(10, 37, 253, 22);
		frame.getContentPane().add(lblIngresarNumeroDel);
		
		txt = new JTextField();
		txt.setBounds(273, 37, 116, 22);
		frame.getContentPane().add(txt);
		txt.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int numero = Integer.parseInt(txt.getText());
				
				if (numero==1)
					lblResultado.setText("El mes es Enero tiene 31 dias");
				else if (numero ==2)
					lblResultado.setText("El mes es Febrero tiene 28 dias");
				else if (numero == 3)
					lblResultado.setText("El mes es Marzo tiene 31 dias");
				else if (numero == 4)
					lblResultado.setText("El mes es Abril tiene 30 dias");
				else if (numero ==5)
					lblResultado.setText("El mes es Mayo tiene 31 dias");
				else if (numero == 6)
					lblResultado.setText("El mes es Junio tiene 30 dias");
				else if (numero == 7)
					lblResultado.setText("El mes es Julio tiene 31 dias");
				else if (numero ==8)
					lblResultado.setText("El mes esAgosto tiene 31 dias");
				else if (numero == 9)
					lblResultado.setText("El mes es Septiembre tiene 30 dias");
				else if (numero == 10)
					lblResultado.setText("El mes es Octubre tiene 31 dias");
				else if (numero ==11)
					lblResultado.setText("El mes es Noviembre tiene 30 dias");
				else if (numero == 12)
					lblResultado.setText("El mes es Diciembre y tiene 31 dias");
				else if (numero >=13)
					lblResultado.setText("No es un mes valido,solo hay 12");
				
			}
		});
		btnCalcular.setForeground(Color.BLACK);
		btnCalcular.setFont(new Font("Microsoft Tai Le", Font.BOLD, 15));
		btnCalcular.setBounds(166, 99, 97, 25);
		frame.getContentPane().add(btnCalcular);
		
		lblResultado = new JLabel("");
		lblResultado.setOpaque(true);
		lblResultado.setBackground(Color.YELLOW);
		lblResultado.setBounds(107, 165, 224, 30);
		frame.getContentPane().add(lblResultado);
	}
}
