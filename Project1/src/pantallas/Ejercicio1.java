package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Ejercicio1 {

	private JFrame frame;
	private JTextField txt1;
	private JTextField txt2;
	private JTextField txt3;
	private JLabel lblPromedio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio1 window = new Ejercicio1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.GRAY);
		frame.setBounds(100, 100, 685, 391);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNota = new JLabel("1era nota");
		lblNota.setFont(new Font("Calibri Light", Font.ITALIC, 20));
		lblNota.setBounds(227, 63, 87, 25);
		frame.getContentPane().add(lblNota);
		
		txt1 = new JTextField();
		txt1.setBounds(391, 65, 116, 22);
		frame.getContentPane().add(txt1);
		txt1.setColumns(10);
		
		txt2 = new JTextField();
		txt2.setText("");
		txt2.setBounds(391, 135, 116, 22);
		frame.getContentPane().add(txt2);
		txt2.setColumns(10);
		
		txt3 = new JTextField();
		txt3.setBounds(391, 204, 116, 22);
		frame.getContentPane().add(txt3);
		txt3.setColumns(10);
		
		JButton btnCalcularPromedio = new JButton("Calcular Promedio");
		btnCalcularPromedio.setBackground(Color.YELLOW);
		btnCalcularPromedio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				float fnota1 = Float.parseFloat(txt1.getText());
				float fnota2 = Float.parseFloat(txt2.getText());
				float fnota3 = Float.parseFloat(txt3.getText());
				
			float fpromedio = (fnota1 + fnota2 + fnota3)/3;
			
			if (fpromedio>=7) {
				lblPromedio.setText("Aprobado") ;
			}
				else lblPromedio.setText("Desaprobado");{
			}
				
				
				
			}
		});
		btnCalcularPromedio.setFont(new Font("Calibri Light", Font.BOLD, 15));
		btnCalcularPromedio.setForeground(Color.DARK_GRAY);
		btnCalcularPromedio.setBounds(109, 254, 205, 42);
		frame.getContentPane().add(btnCalcularPromedio);
		
		lblPromedio = new JLabel("     Promedio");
		lblPromedio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblPromedio.setOpaque(true);
		lblPromedio.setBackground(Color.WHITE);
		lblPromedio.setForeground(new Color(0, 0, 0));
		lblPromedio.setBounds(448, 256, 124, 33);
		frame.getContentPane().add(lblPromedio);
		
		JLabel lbldaNota = new JLabel("2da nota");
		lbldaNota.setFont(new Font("Calibri Light", Font.ITALIC, 20));
		lbldaNota.setBounds(227, 133, 87, 25);
		frame.getContentPane().add(lbldaNota);
		
		JLabel lbleraNota = new JLabel("3era nota");
		lbleraNota.setFont(new Font("Calibri Light", Font.ITALIC, 20));
		lbleraNota.setBounds(227, 202, 87, 25);
		frame.getContentPane().add(lbleraNota);
	}
}

